#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

function install() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
}

echo -e "♻️\tChecking ${PWD} == ${CI_PROJECT_DIR}"

function clone_tutorials() {
  sudo pacman --needed --noconfirm -S git >/dev/null 2>&1

  git config --global advice.detachedHead false
  local BASE_URL=https://github.com/precice
  local PRECICE_TUTORIALS_REPO=$BASE_URL/tutorials

  echo -e "Cloning ${PRECICE_TUTORIALS_REPO}"

  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch v202211.0 \
    ${PRECICE_TUTORIALS_REPO}.git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/tutorials"
  ls -l ${CI_PROJECT_DIR}/tutorials
  sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function heatTransfer() {
  pushd "${CI_PROJECT_DIR}"/tutorials/partitioned-heat-conduction/openfoam-solver
  local _openfoam_version=$(pacman -Q openfoam-com | sed -e 's/.* //; s/-.*//g')
  source /opt/OpenFOAM/OpenFOAM-${_openfoam_version}/etc/bashrc || true
  # https://github.com/precice/vm/blob/main/provisioning/install-openfoam.sh#L41
  # ~/OpenFOAM/user-v2112/platforms/linux64GccDPInt32Opt/bin/heatTransfer
  wmake
  popd
}

function run_partitioned_heat_conduction() {
  pushd "${CI_PROJECT_DIR}"/tutorials/partitioned-heat-conduction/fenics
  ls -l
  ./run.sh -d &
  local PID_A=$!
  # ./clean.sh
  popd
  pushd "${CI_PROJECT_DIR}"/tutorials/partitioned-heat-conduction/openfoam-neumann
  ls -l
  ./run.sh &
  local PID_B=$!
  printf "Finished =)"
  ls -l
  popd
  wait $PID_A $PID_B
}

function log() {
  mkdir -p ${CI_PROJECT_DIR}/log
  run_partitioned_heat_conduction 2>&1 | tee -a ${CI_PROJECT_DIR}/log/log_tutorial_phc_fenicsprecice_openfoam-com_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/log/output_tutorial_phc_fenicsprecice_openfoam-com_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').tar.zst ${CI_PROJECT_DIR}/tutorials/partitioned-heat-conduction/{fenics,openfoam-neumann}
  # ls -l ${CI_PROJECT_DIR}/log/*.log
}

function visualize() {
  # https://raw.githubusercontent.com/precice/vm/main/provisioning/.alias
  # TODO: Check SC2002 (style): Useless cat. Consider 'cmd < file | ..' or 'cmd file | ..' instead.
  local CONFIG_PATH="${CI_PROJECT_DIR}"/tutorials/partitioned-heat-conduction/precice-config.xml
  # precice-tools check ${CONFIG_PATH} # FIXME: Try in gitpod or local
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpdf >${CI_PROJECT_DIR}/log/precice-config.pdf
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tsvg >${CI_PROJECT_DIR}/log/precice-config.svg
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpng >${CI_PROJECT_DIR}/log/precice-config.png
}

curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/install_openfoam-com.sh | bash
install
clone_tutorials
heatTransfer
log
visualize
ls "${CI_PROJECT_DIR}"/log/*.log "${CI_PROJECT_DIR}"/log/*.pdf "${CI_PROJECT_DIR}"/log/*.png "${CI_PROJECT_DIR}"/log/*.svg
