#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

function install() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
}

echo -e "♻️\tChecking ${PWD} == ${CI_PROJECT_DIR}"

function clone_tutorials() {
  sudo pacman --needed --noconfirm -S git >/dev/null 2>&1

  git config --global advice.detachedHead false
  local BASE_URL=https://github.com/precice
  local PRECICE_TUTORIALS_REPO=$BASE_URL/tutorials

  echo -e "Cloning ${PRECICE_TUTORIALS_REPO}"

  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch v202211.0 \
    ${PRECICE_TUTORIALS_REPO}.git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/tutorials"
  ls -l ${CI_PROJECT_DIR}/tutorials
  sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function apply_patch() {
  sudo pacman --needed --noconfirm -S git >/dev/null 2>&1
  local PATCH_COMMIT="https://github.com/precice/tutorials/commit/3f647c07e69a3e6096e61351476be4a3ccefa21d.patch"
  # https://github.com/precice/tutorials/pull/295.patch
  pushd "${CI_PROJECT_DIR}"/tutorials
  curl -O ${PATCH_COMMIT}
  git config --global user.email dune-archiso@gitlab.com
  git config --global user.name dune-archiso
  # https://stackoverflow.com/q/14509950
  git am -3 --whitespace=fix --signoff <*.patch
  popd
  sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function run_partitioned_heat_conduction() {
  sudo pacman --needed --noconfirm -S openssh >/dev/null 2>&1
  pushd "${CI_PROJECT_DIR}"/tutorials/partitioned-heat-conduction/nutils
  ls -l
  ./run.sh -d &
  local PID_A=$!
  # ./clean.sh
  popd
  pushd "${CI_PROJECT_DIR}"/tutorials/partitioned-heat-conduction/nutils
  ls -l
  ./run.sh -n &
  local PID_B=$!
  printf "Finished =)"
  ls -l
  popd
  wait $PID_A $PID_B
  sudo pacman -Rscn --noconfirm openssh >/dev/null 2>&1
}

function log() {
  mkdir -p ${CI_PROJECT_DIR}/log
  run_partitioned_heat_conduction 2>&1 | tee -a ${CI_PROJECT_DIR}/log/log_tutorial_phc_nutils_nutils_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/log/output_tutorial_phc_nutils_nutils_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').tar.zst ${CI_PROJECT_DIR}/tutorials/partitioned-heat-conduction/nutils
  mv ~/public_html/heat.py/log.html ${CI_PROJECT_DIR}/log
  # ls -l ${CI_PROJECT_DIR}/log/*.log
}

function visualize() {
  # https://raw.githubusercontent.com/precice/vm/main/provisioning/.alias
  # TODO: Check SC2002 (style): Useless cat. Consider 'cmd < file | ..' or 'cmd file | ..' instead.
  local CONFIG_PATH="${CI_PROJECT_DIR}"/tutorials/partitioned-heat-conduction/precice-config.xml
  # precice-tools check ${CONFIG_PATH} # FIXME: Try in gitpod or local
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpdf >${CI_PROJECT_DIR}/log/precice-config.pdf
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tsvg >${CI_PROJECT_DIR}/log/precice-config.svg
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpng >${CI_PROJECT_DIR}/log/precice-config.png
}

install
clone_tutorials
apply_patch
run_partitioned_heat_conduction
# log
# visualize
ls "${CI_PROJECT_DIR}"/log/*.log "${CI_PROJECT_DIR}"/log/*.pdf "${CI_PROJECT_DIR}"/log/*.png "${CI_PROJECT_DIR}"/log/*.svg
