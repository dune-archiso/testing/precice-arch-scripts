# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

#################################
# * Test pyprecice preCICE
#################################

test_pyprecice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 5 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/pyprecice/pyprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs: ["precice", "python-pyprecice", "precice-config-visualizer-git"]

#################################
# * Test solverdummy preCICE
#################################

test_solverdummy_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 5 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/dummy/solverdummy.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs: ["precice", "precice-config-visualizer-git"]

#################################
# * Test tutorial precice-precice preCICE
# https://precice.org/tutorials-elastic-tube-1d.html
#################################

test_tutorial_et1_precice-precice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 5 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/elastic-tube-1d/test_tutorial_et1_precice_precice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs: ["precice", "precice-config-visualizer-git"]

#################################
# * Test tutorial precice-pyprecice preCICE
# https://precice.org/tutorials-elastic-tube-1d.html
#################################

test_tutorial_et1_precice-pyprecice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 5 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/elastic-tube-1d/test_tutorial_et1_precice_pyprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs: ["precice", "python-pyprecice", "precice-config-visualizer-git"]

#################################
# * Test tutorial pyprecice-precice preCICE
# https://precice.org/tutorials-elastic-tube-1d.html
#################################

test_tutorial_et1_pyprecice-precice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 5 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/elastic-tube-1d/test_tutorial_et1_pyprecice_precice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs: ["precice", "python-pyprecice", "precice-config-visualizer-git"]

#################################
# * Test tutorial pyprecice-pyprecice preCICE
# https://precice.org/tutorials-elastic-tube-1d.html
#################################

test_tutorial_et1_pyprecice-pyprecice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 5 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/elastic-tube-1d/test_tutorial_et1_pyprecice_pyprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs: ["precice", "python-pyprecice", "precice-config-visualizer-git"]

#################################
# * Test tutorial openfoam-com-deal-ii preCICE
# https://precice.org/tutorials-elastic-tube-3d.html
#################################

test_tutorial_et3_openfoam-com-calculix_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 60 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/elastic-tube-3d/test_tutorial_et3_openfoam-com_calculix.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "calculix-precice",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com-deal-ii preCICE
# https://precice.org/tutorials-elastic-tube-3d.html
#################################

test_tutorial_et3_openfoam-com-fenicsprecice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  allow_failure: true
  timeout: 120 minutes
  variables:
    ISARCH4EDU: "true"
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/elastic-tube-3d/test_tutorial_et3_openfoam-com_fenicsprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "python-pyprecice",
      "python-fenicsprecice",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com-fenicsprecice preCICE
# https://precice.org/tutorials-flow-over-heated-plate.html
#################################

test_tutorial_fohp_openfoam-com-fenicsprecice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 10 minutes
  variables:
    ISARCH4EDU: "true"
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/flow-over-heated-plate/test_tutorial_fohp_openfoam-com_fenicsprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "python-pyprecice",
      "python-fenicsprecice",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com-nutils preCICE
# https://precice.org/tutorials-flow-over-heated-plate.html
#################################

test_tutorial_fohp_openfoam-com-nutils_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 10 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/flow-over-heated-plate/test_tutorial_fohp_openfoam-com_nutils.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "python-pyprecice",
      "python-bottombar",
      "python-treelog",
      "python-stringly",
      "python-nutils",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com-openfoam-com preCICE
# https://precice.org/tutorials-flow-over-heated-plate.html
#################################

test_tutorial_fohp_openfoam-com-openfoam-com_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 10 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/flow-over-heated-plate/test_tutorial_fohp_openfoam-com_openfoam-com.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs: ["precice", "openfoam-com-precice", "precice-config-visualizer-git"]

#################################
# * Test tutorial openfoam-com-openfoam-com preCICE
# https://precice.org/tutorials-flow-over-heated-plate-nearest-projection.html
#################################

test_tutorial_fohpnp_openfoam-com-openfoam-com_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 10 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/flow-over-heated-plate-nearest-projection/test_tutorial_fohpnp_openfoam-com_openfoam-com.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs: ["precice", "openfoam-com-precice", "precice-config-visualizer-git"]

#################################
# * Test tutorial openfoam-com_parallel-deal-ii preCICE
# https://precice.org/tutorials-multiple-perpendicular-flaps.html
#################################

test_tutorial_mpf_openfoam-com_parallel-deal-ii_precice-stable:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 40 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/multiple-perpendicular-flaps/test_tutorial_mpf_openfoam-com_parallel_deal-ii.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "openfoam-com-precice",
      "deal-ii-precice-stable-git",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com_serial-deal-ii preCICE
# https://precice.org/tutorials-multiple-perpendicular-flaps.html
#################################

test_tutorial_mpf_openfoam-com_serial-deal-ii_precice-stable:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 20 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/multiple-perpendicular-flaps/test_tutorial_mpf_openfoam-com_serial_deal-ii.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "openfoam-com-precice",
      "deal-ii-precice-stable-git",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-dune preCICE
# https://precice.org/tutorials-perpendicular-flap.html
#################################

test_tutorial_pf_openfoam-com-dune_precice-stable:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 10 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n[dune-makepkg]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/testing/aur/dune-makepkg/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/perpendicular-flap/openfoam-com_dune.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "dune-elastodynamics-git",
      "precice",
      "dune-precice-stable-git",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com-fenicsprecice preCICE
# https://precice.org/tutorials-perpendicular-flap.html
#################################

test_tutorial_pf_openfoam-com-fenicsprecice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 10 minutes
  variables:
    ISARCH4EDU: "true"
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/perpendicular-flap/fluid-openfoam-com_solid-fenicsprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "python-pyprecice",
      "python-fenicsprecice",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial su2-calculix preCICE
# https://precice.org/tutorials-perpendicular-flap.html
#################################

test_tutorial_pf_su2-calculix_precice-stable:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 20 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/perpendicular-flap/su2_calculix.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "su2-precice-stable-git",
      "precice",
      "calculix-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial su2-deal-ii preCICE
# https://precice.org/tutorials-perpendicular-flap.html
#################################

test_tutorial_pf_su2-deal-ii_precice-stable:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 15 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/perpendicular-flap/su2_deal-ii.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "su2-precice-stable-git",
      "precice",
      "deal-ii-precice-stable-git",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial su2-dune preCICE
# https://precice.org/tutorials-perpendicular-flap.html
#################################

test_tutorial_pf_su2-dune_precice-stable:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 120 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n[dune-makepkg]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/testing/aur/dune-makepkg/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/perpendicular-flap/su2_dune.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "su2-precice-stable-git",
      "dune-elastodynamics-git",
      "precice",
      "dune-precice-stable-git",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial su2-fenicsprecice preCICE
# https://precice.org/tutorials-perpendicular-flap.html
#################################

test_tutorial_pf_su2-fenicsprecice_precice-stable:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 120 minutes
  variables:
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "precice-*.pkg.tar.zst python-pyprecice-*.pkg.tar.zst python-fenicsprecice-*.pkg.tar.zst" # scotch petsc python-dijitso python-fiat python-ufl python-ffc dolfin python-dolfin
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/perpendicular-flap/su2_fenicsprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "su2-precice-stable-git",
      "precice",
      "python-pyprecice",
      "python-fenicsprecice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial fenicsprecice-fenicsprecice preCICE
# https://precice.org/tutorials-partitioned-heat-conduction.html
#################################

test_tutorial_phc_fenicsprecice-fenicsprecice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 120 minutes
  variables:
    # ISARCH4EDU: "true"
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "precice-*.pkg.tar.zst python-pyprecice-*.pkg.tar.zst python-fenicsprecice-*.pkg.tar.zst" # scotch petsc python-dijitso python-fiat python-ufl python-ffc dolfin python-dolfin
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/partitioned-heat-conduction/test_tutorial_phc_fenicsprecice_fenicsprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "python-pyprecice",
      "python-fenicsprecice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial fenicsprecice-nutils preCICE
# https://precice.org/tutorials-partitioned-heat-conduction.html
#################################

test_tutorial_phc_fenicsprecice-nutils_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  timeout: 120 minutes
  variables:
    ISDEPENDS: "true"
    DEPENDENCIES_PACKAGE: "precice-*.pkg.tar.zst python-pyprecice-*.pkg.tar.zst python-fenicsprecice-*.pkg.tar.zst" # scotch petsc python-dijitso python-fiat python-ufl python-ffc dolfin python-dolfin
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/partitioned-heat-conduction/test_tutorial_phc_fenicsprecice_nutils.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "python-pyprecice",
      "python-fenicsprecice",
      "python-bottombar",
      "python-treelog",
      "python-stringly",
      "python-nutils",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial fenicsprecice-openfoam-com preCICE
# https://precice.org/tutorials-partitioned-heat-conduction.html
#################################

test_tutorial_phc_fenicsprecice-openfoam-com_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  allow_failure: true
  timeout: 120 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/partitioned-heat-conduction/test_tutorial_phc_fenicsprecice_openfoam-com.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "swak4foam-hg",
      "precice",
      "python-pyprecice",
      "python-fenicsprecice",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial nutils-fenicsprecice preCICE
# https://precice.org/tutorials-partitioned-heat-conduction.html
#################################

test_tutorial_phc_nutils-fenicsprecice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  allow_failure: true
  timeout: 120 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/partitioned-heat-conduction/test_tutorial_phc_nutils_fenicsprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "python-pyprecice",
      "python-fenicsprecice",
      "python-bottombar",
      "python-treelog",
      "python-stringly",
      "python-nutils",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial nutils-nutils preCICE
# https://precice.org/tutorials-partitioned-heat-conduction.html
#################################

test_tutorial_phc_nutils-nutils_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  allow_failure: true
  timeout: 120 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/partitioned-heat-conduction/test_tutorial_phc_nutils_nutils.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "precice",
      "python-pyprecice",
      "python-bottombar",
      "python-treelog",
      "python-stringly",
      "python-nutils",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial nutils-openfoam-com preCICE
# https://precice.org/tutorials-partitioned-heat-conduction.html
#################################

test_tutorial_phc_nutils-openfoam-com_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  allow_failure: true
  timeout: 120 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/partitioned-heat-conduction/test_tutorial_phc_nutils_openfoam-com.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "swak4foam-hg",
      "precice",
      "python-pyprecice",
      "python-bottombar",
      "python-treelog",
      "python-stringly",
      "python-nutils",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com-fenicsprecice preCICE
# https://precice.org/tutorials-partitioned-heat-conduction.html
#################################

test_tutorial_phc_openfoam-com-fenicsprecice_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  variables:
    ISARCH4EDU: "true"
  allow_failure: true
  timeout: 120 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/partitioned-heat-conduction/test_tutorial_phc_openfoam-com_fenicsprecice.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "swak4foam-hg",
      "precice",
      "python-pyprecice",
      "python-fenicsprecice",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com-nutils preCICE
# https://precice.org/tutorials-partitioned-heat-conduction.html
#################################

test_tutorial_phc_openfoam-com-nutils_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  allow_failure: true
  timeout: 120 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/partitioned-heat-conduction/test_tutorial_phc_openfoam-com_nutils.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "swak4foam-hg",
      "precice",
      "python-pyprecice",
      "python-bottombar",
      "python-treelog",
      "python-stringly",
      "python-nutils",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com-openfoam-com preCICE
# https://precice.org/tutorials-partitioned-heat-conduction.html
#################################

test_tutorial_phc_openfoam-com-openfoam-com_precice:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  allow_failure: true
  timeout: 120 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/partitioned-heat-conduction/test_tutorial_phc_openfoam-com_openfoam-com.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "swak4foam-hg",
      "precice",
      "openfoam-com-precice",
      "precice-config-visualizer-git",
    ]

#################################
# * Test tutorial openfoam-com-deal-ii preCICE
# https://precice.org/tutorials-turek-hron-fsi3.html
#################################

test_tutorial_thf_openfoam-com-deal-ii_precice-stable:
  image: registry.gitlab.com/dune-archiso/images/dune-archiso:latest
  tags: [saas-linux-large-amd64]
  stage: start
  allow_failure: true
  timeout: 175 minutes
  before_script:
    - echo -e '\n[dune-archiso-repository-core]\nSigLevel = Required DatabaseOptional\nServer = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-core/$arch\n' | sudo tee -a /etc/pacman.conf
  script:
    - curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/turek-hron-fsi3/test_tutorial_thf_openfoam-com_deal-ii.sh | bash -e -x
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/log
  needs:
    [
      "swak4foam-hg",
      "precice",
      "openfoam-com-precice",
      "deal-ii-precice-stable-git",
      "precice-config-visualizer-git",
    ]
