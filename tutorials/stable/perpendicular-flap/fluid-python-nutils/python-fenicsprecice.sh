#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2143

function rollback() {
  # https://github.com/precice/tutorials/issues/217
  local ROLLBACK=$(echo -e "from nutils import function\ntry: function.replace()\nexcept AttributeError as error:\n\tprint('true')" | python)
  if [ "$ROLLBACK" == "true" ]; then
    sudo pacman -Rscn --noconfirm python-nutils >/dev/null 2>&1
    sudo pacman --needed --noconfirm -S python-pip >/dev/null 2>&1
    python -m venv --system-site-packages nutils-env
    . nutils-env/bin/activate
    pip install nutils==6.3
    pip show nutils
    sudo pacman -Rscn --noconfirm python-pip >/dev/null 2>&1
  fi
}

function install() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  # https://fenicsproject.discourse.group/t/mumps-and-umfpack-not-available-as-lu-solvers-on-fenics-2019-2-0-dev0/5692/3
  sudo pacman -S suitesparse mumps --needed --noconfirm >/dev/null 2>&1 #suitesparse
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
  rollback
}

echo -e "♻️\tChecking ${PWD} == ${CI_PROJECT_DIR}"

function clone_tutorials() {
  sudo pacman --needed --noconfirm -S git >/dev/null 2>&1

  git config --global advice.detachedHead false
  local BASE_URL=https://github.com/precice
  local PRECICE_TUTORIALS_REPO="${BASE_URL}"/tutorials

  echo -e "Cloning ${PRECICE_TUTORIALS_REPO}"

  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch v202211.0 \
    "${PRECICE_TUTORIALS_REPO}".git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/tutorials"

  ls -l "${CI_PROJECT_DIR}"/tutorials

  sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function apply_patch() {
  # https://docs.nutils.org/en/v7.2/nutils/evaluable/#nutils.evaluable.replace
  # https://docs.nutils.org/en/v6.3/nutils/function/#nutils.function.replace
  # https://github.com/precice/tutorials/issues/217
  sed -i 's/, cli/, cli, evaluable/' "${CI_PROJECT_DIR}"/tutorials/perpendicular-flap/fluid-nutils/fluid.py
  sed -i 's/@function.replace/@evaluable.replace/' "${CI_PROJECT_DIR}"/tutorials/perpendicular-flap/fluid-nutils/fluid.py
}

function run_perpendicular_flap() {
  sudo pacman --needed --noconfirm -S openssh python-matplotlib >/dev/null 2>&1

  pushd "${CI_PROJECT_DIR}"/tutorials/perpendicular-flap/solid-fenics || exit
  ./run.sh &
  local PID_A=$!
  # ./clean.sh
  popd || exit
  pushd "${CI_PROJECT_DIR}"/tutorials/perpendicular-flap/fluid-nutils || exit
  ls -l
  python fluid.py #|| true
  # cat ~/public_html/fluid.py/log.html
  ./run.sh &
  local PID_B=$!
  popd || exit
  wait "${PID_A}" "${PID_B}"
  sudo pacman -Rscn --noconfirm openssh python-matplotlib >/dev/null 2>&1
}

function log() {
  mkdir -p ${CI_PROJECT_DIR}/log
  run_perpendicular_flap 2>&1 | tee -a ${CI_PROJECT_DIR}/log/perpendicular-flap_fluid-nutils_solid-fenics_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/log/output_perpendicular-flap_fluid-nutils_solid-fenics_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').tar.zst ${CI_PROJECT_DIR}/tutorials/perpendicular-flap/{solid-fenics,fluid-nutils}
  # ls -l ${CI_PROJECT_DIR}/log/*.log
}

function plot_displacement() {
  sudo pacman --needed --noconfirm -S gnuplot texlive-pictures >/dev/null 2>&1
  echo "set terminal postscript eps enhanced color font 'Helvetica,10'" >>~/.gnuplot
  cat ~/.gnuplot
  pushd "${CI_PROJECT_DIR}"/tutorials/perpendicular-flap
  ls -l plot-displacement.sh
  ./plot-displacement.sh solid-fenics >${CI_PROJECT_DIR}/log/plot_displacement_perpendicular-flap_fluid-nutils_solid-fenics_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').ps
  ls -l
  epspdf ${CI_PROJECT_DIR}/log/*.ps
  rm ${CI_PROJECT_DIR}/log/*.ps
  popd
  sudo pacman -Rscn --noconfirm gnuplot texlive-pictures >/dev/null 2>&1
}

function visualize() {
  # https://raw.githubusercontent.com/precice/vm/main/provisioning/.alias
  # TODO: Check SC2002 (style): Useless cat. Consider 'cmd < file | ..' or 'cmd file | ..' instead.
  local CONFIG_PATH="${CI_PROJECT_DIR}"/tutorials/perpendicular-flap/precice-config.xml
  precice-tools check ${CONFIG_PATH}
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpdf >${CI_PROJECT_DIR}/log/precice-config.pdf
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tsvg >${CI_PROJECT_DIR}/log/precice-config.svg
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpng >${CI_PROJECT_DIR}/log/precice-config.png
}

curl -s https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/add_arch4edu.sh | bash
install
clone_tutorials
# apply_patch
log
plot_displacement
visualize
ls "${CI_PROJECT_DIR}"/log/*.log "${CI_PROJECT_DIR}"/log/*.pdf "${CI_PROJECT_DIR}"/log/*.png "${CI_PROJECT_DIR}"/log/*.svg
