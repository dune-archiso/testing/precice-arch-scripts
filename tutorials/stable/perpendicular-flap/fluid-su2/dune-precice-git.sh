#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

function install() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
}

echo -e "♻️\tChecking ${PWD} == ${CI_PROJECT_DIR}"

function clone_tutorials() {
  git config --global advice.detachedHead false
  local BASE_URL=https://github.com/precice
  local PRECICE_TUTORIALS_REPO=$BASE_URL/tutorials

  echo -e "Cloning ${PRECICE_TUTORIALS_REPO}"

  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch v202211.0 \
    ${PRECICE_TUTORIALS_REPO}.git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/tutorials"
  ls -l ${CI_PROJECT_DIR}/tutorials
}

function run_perpendicular_flap() {
  sudo pacman --needed --noconfirm -S openssh >/dev/null 2>&1
  # local EXECUTABLE_DIR="${CI_PROJECT_DIR}"/build/examples
  pushd "${CI_PROJECT_DIR}"/tutorials/perpendicular-flap/solid-dune || exit
  cp /usr/bin/dune-perpendicular-flap .
  ls -l
  # mv "${EXECUTABLE_DIR}"/dune-perpendicular-flap .
  ./run.sh &
  local PID_A=$!
  # ./clean.sh
  popd || exit
  pushd "${CI_PROJECT_DIR}"/tutorials/perpendicular-flap/fluid-su2 || exit
  export SU2_RUN=/usr/bin
  ls -l
  ./run.sh &
  local PID_B=$!
  popd || exit
  wait "${PID_A}" "${PID_B}"
  sudo pacman -Rscn --noconfirm openssh >/dev/null 2>&1
}

function log() {
  mkdir -p "${CI_PROJECT_DIR}"/log
  run_perpendicular_flap 2>&1 | tee -a "${CI_PROJECT_DIR}"/log/log_tutorial_pf_su2_dune_"$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago')".log >/dev/null 2>&1
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/log/output_tutorial_pf_su2_dune_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').tar.zst ${CI_PROJECT_DIR}/tutorials/perpendicular-flap/{solid-dune,fluid-su2}
  # ls -l ${CI_PROJECT_DIR}/log/*.log
}

function plot_displacement() {
  sudo pacman --needed --noconfirm -S gnuplot texlive-pictures >/dev/null 2>&1
  echo "set terminal postscript eps enhanced color font 'Helvetica,10'" >>~/.gnuplot
  cat ~/.gnuplot
  pushd "${CI_PROJECT_DIR}"/tutorials/perpendicular-flap
  ls -l plot-displacement.sh
  ./plot-displacement.sh solid-dune >${CI_PROJECT_DIR}/log/plot_displacement_tutorial_pf_su2_dune_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').ps
  ls -l
  epspdf ${CI_PROJECT_DIR}/log/*.ps
  rm ${CI_PROJECT_DIR}/log/*.ps
  popd
  sudo pacman -Rscn --noconfirm gnuplot texlive-pictures >/dev/null 2>&1
}

function visualize() {
  # https://raw.githubusercontent.com/precice/vm/main/provisioning/.alias
  # TODO: Check SC2002 (style): Useless cat. Consider 'cmd < file | ..' or 'cmd file | ..' instead.
  local CONFIG_PATH="${CI_PROJECT_DIR}"/tutorials/perpendicular-flap/precice-config.xml
  # precice-tools check ${CONFIG_PATH} # FIXME: Try in gitpod or local
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpdf >${CI_PROJECT_DIR}/log/precice-config.pdf
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tsvg >${CI_PROJECT_DIR}/log/precice-config.svg
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpng >${CI_PROJECT_DIR}/log/precice-config.png
}

install
clone_tutorials
log
plot_displacement
visualize
ls "${CI_PROJECT_DIR}"/log/*.log "${CI_PROJECT_DIR}"/log/*.pdf "${CI_PROJECT_DIR}"/log/*.png "${CI_PROJECT_DIR}"/log/*.svg
