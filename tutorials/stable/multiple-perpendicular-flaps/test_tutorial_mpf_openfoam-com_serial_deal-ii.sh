#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC1090,SC2143,SC2155

function install() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
}

echo -e "♻️\tChecking ${PWD} == ${CI_PROJECT_DIR}"

function clone_tutorials() {
  sudo pacman --needed --noconfirm -S git >/dev/null 2>&1

  git config --global advice.detachedHead false
  local BASE_URL=https://github.com/precice
  local PRECICE_TUTORIALS_REPO="${BASE_URL}"/tutorials

  echo -e "Cloning ${PRECICE_TUTORIALS_REPO}"

  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch v202211.0 \
    "${PRECICE_TUTORIALS_REPO}".git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/tutorials"
  ls -l "${CI_PROJECT_DIR}"/tutorials
  sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function run_multiple_perpendicular_flaps() {
  sudo pacman --needed --noconfirm -S openssh >/dev/null 2>&1
  pushd "${CI_PROJECT_DIR}"/tutorials/multiple-perpendicular-flaps/solid-left-dealii || exit
  cp /usr/bin/elasticity .
  ls -l
  ./run.sh -linear &
  local PID_A=$!
  # ./clean.sh
  popd || exit
  pushd "${CI_PROJECT_DIR}"/tutorials/multiple-perpendicular-flaps/solid-right-dealii || exit
  cp /usr/bin/elasticity .
  ls -l
  ./run.sh -linear &
  local PID_B=$!
  # ./clean.sh
  popd || exit
  pushd "${CI_PROJECT_DIR}"/tutorials/multiple-perpendicular-flaps/fluid-openfoam || exit
  local _openfoam_version=$(pacman -Q openfoam-com | sed -e 's/.* //; s/-.*//g')
  source /opt/OpenFOAM/OpenFOAM-"${_openfoam_version}"/etc/bashrc || true
  ls -l
  ./run.sh &
  local PID_C=$!
  printf "Finished =)"
  ls -l
  popd || exit
  wait "${PID_A}" "${PID_B}" "${PID_C}"
  sudo pacman -Rscn --noconfirm openssh >/dev/null 2>&1
}

function log() {
  mkdir -p "${CI_PROJECT_DIR}"/log
  run_multiple_perpendicular_flaps 2>&1 | tee -a "${CI_PROJECT_DIR}"/log/log_tutorial_mpf_serial_openfoam-com_deal-ii_"$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago')".log >/dev/null 2>&1
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/log/output_tutorial_mpf_serial_openfoam-com_deal-ii_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').tar.zst ${CI_PROJECT_DIR}/tutorials/multiple-perpendicular-flaps/{solid-left-dealii,solid-right-dealii,fluid-openfoam}
  # ls -l ${CI_PROJECT_DIR}/log/*.log
}

# function plot_displacement() {
#   sudo pacman --needed --noconfirm -S gnuplot texlive-pictures >/dev/null 2>&1
#   echo "set terminal postscript eps enhanced color font 'Helvetica,10'" >>~/.gnuplot
#   cat ~/.gnuplot
#   pushd "${CI_PROJECT_DIR}"/tutorials/perpendicular-flap
#   ls -l plot-displacement.sh
#   ./plot-displacement.sh solid-dealii >${CI_PROJECT_DIR}/log/plot_displacement_tutorial_pf_openfoam-com_deal-ii_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').ps
#   ls -l
#   epspdf ${CI_PROJECT_DIR}/log/*.ps
#   rm ${CI_PROJECT_DIR}/log/*.ps
#   popd
#   sudo pacman -Rscn --noconfirm gnuplot texlive-pictures >/dev/null 2>&1
# }

curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/install_openfoam-com.sh | bash
curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/install_deal-ii.sh | bash
install
clone_tutorials
log
# plot_displacement
