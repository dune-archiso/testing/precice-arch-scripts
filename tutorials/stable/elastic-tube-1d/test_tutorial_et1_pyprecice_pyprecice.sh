#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

function install() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
}

echo -e "♻️\tChecking ${PWD} == ${CI_PROJECT_DIR}"

function clone_tutorials() {
  sudo pacman --needed --noconfirm -S git >/dev/null 2>&1

  git config --global advice.detachedHead false
  local BASE_URL=https://github.com/precice
  local PRECICE_TUTORIALS_REPO=$BASE_URL/tutorials

  echo -e "Cloning ${PRECICE_TUTORIALS_REPO}"

  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch v202211.0 \
    ${PRECICE_TUTORIALS_REPO}.git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/tutorials"
  ls -l ${CI_PROJECT_DIR}/tutorials
  sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function run_elastic_tube_1d() {
  sudo pacman --needed --noconfirm -S openssh python-matplotlib >/dev/null 2>&1

  pushd "${CI_PROJECT_DIR}"/tutorials/elastic-tube-1d/fluid-python
  ls -l
  ./run.sh &
  local PID_A=$!
  # ./clean.sh
  popd
  pushd "${CI_PROJECT_DIR}"/tutorials/elastic-tube-1d/solid-python
  ls -l
  ./run.sh &
  local PID_B=$!
  printf "Finished =)"
  ls -l
  popd
  wait $PID_A $PID_B
  sudo pacman -Rscn --noconfirm openssh python-matplotlib >/dev/null 2>&1
}

function log() {
  mkdir -p ${CI_PROJECT_DIR}/log
  run_elastic_tube_1d 2>&1 | tee -a ${CI_PROJECT_DIR}/log/log_tutorial_et1_pyprecice_pyprecice_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/log/output_tutorial_et1_pyprecice_pyprecice_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').tar.zst ${CI_PROJECT_DIR}/tutorials/elastic-tube-1d/{fluid-python,solid-python}
  # ls -l ${CI_PROJECT_DIR}/log/*.log
}

function plot_diameter() {
  sudo pacman --needed --noconfirm -S python-matplotlib vtk ffmpeg python-mpi4py fmt pdal glew ospray qt5-base openvr unixodbc liblas cgns adios2 libharu gl2ps postgresql-libs netcdf mariadb-libs >/dev/null 2>&1
  pushd "${CI_PROJECT_DIR}"/tutorials/elastic-tube-1d
  ls -l plot-diameter.sh
  sed -i '73 a plt.savefig("plot-vtk.pdf")' plot-vtk.py
  MPLBACKEND=Agg python plot-vtk.py diameter fluid-python/output/out_fluid_
  ls -l
  mv ${CI_PROJECT_DIR}/tutorials/elastic-tube-1d/plot-vtk.pdf ${CI_PROJECT_DIR}/log/plot-diameter_tutorial_et1_pyprecice_pyprecice_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').pdf
  popd
  sudo pacman -Rscn --noconfirm python-matplotlib vtk ffmpeg python-mpi4py fmt pdal glew ospray qt5-base openvr unixodbc liblas cgns adios2 libharu gl2ps postgresql-libs netcdf mariadb-libs >/dev/null 2>&1
}

function visualize() {
  # https://raw.githubusercontent.com/precice/vm/main/provisioning/.alias
  # TODO: Check SC2002 (style): Useless cat. Consider 'cmd < file | ..' or 'cmd file | ..' instead.
  local CONFIG_PATH="${CI_PROJECT_DIR}"/tutorials/elastic-tube-1d/precice-config.xml
  precice-tools check ${CONFIG_PATH}
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpdf >${CI_PROJECT_DIR}/log/precice-config.pdf
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tsvg >${CI_PROJECT_DIR}/log/precice-config.svg
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpng >${CI_PROJECT_DIR}/log/precice-config.png
}

install
clone_tutorials
log
plot_diameter
visualize
ls "${CI_PROJECT_DIR}"/log/*.log "${CI_PROJECT_DIR}"/log/*.pdf "${CI_PROJECT_DIR}"/log/*.png "${CI_PROJECT_DIR}"/log/*.svg
