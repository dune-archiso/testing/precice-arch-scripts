#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

function install() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  # ls -l "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
}

echo -e "♻️\tChecking ${PWD} == ${CI_PROJECT_DIR}"

function clone_tutorials() {
  sudo pacman --needed --noconfirm -S git >/dev/null 2>&1

  git config --global advice.detachedHead false
  local BASE_URL=https://github.com/precice
  local PRECICE_TUTORIALS_REPO=$BASE_URL/tutorials

  echo -e "Cloning ${PRECICE_TUTORIALS_REPO}"

  git clone \
    -q \
    --filter=blob:none \
    --depth=1 \
    --branch v202211.0 \
    ${PRECICE_TUTORIALS_REPO}.git

  echo -e "🔎\tListing the files inside of ${CI_PROJECT_DIR}/tutorials"
  ls -l ${CI_PROJECT_DIR}/tutorials
  sudo pacman -Rscn --noconfirm git >/dev/null 2>&1
}

function run_elastic_tube_3d() {
  sudo pacman --needed --noconfirm -S openssh python-mshr >/dev/null 2>&1

  pushd "${CI_PROJECT_DIR}"/tutorials/elastic-tube-3d/solid-fenics
  ls -l
  ./run.sh &
  local PID_A=$!
  # ./clean.sh
  popd
  pushd "${CI_PROJECT_DIR}"/tutorials/elastic-tube-3d/fluid-openfoam
  local _openfoam_version=$(pacman -Q openfoam-com | sed -e 's/.* //; s/-.*//g')
  source /opt/OpenFOAM/OpenFOAM-${_openfoam_version}/etc/bashrc || true
  ls -l
  ./run.sh &
  local PID_B=$!
  printf "Finished =)"
  ls -l
  popd
  wait $PID_A $PID_B
  sudo pacman -Rscn --noconfirm openssh python-mshr >/dev/null 2>&1
}

function plot_displacement() {
  sudo pacman --needed --noconfirm -S gnuplot texlive-pictures >/dev/null 2>&1
  echo "set terminal postscript eps enhanced color font 'Helvetica,10'" >>~/.gnuplot
  cat ~/.gnuplot
  pushd "${CI_PROJECT_DIR}"/tutorials/elastic-tube-3d
  ls -l plot-all-displacements.sh
  sed -i '/solid-calculix/,+2 s/^/#/' plot-all-displacements.sh
  # https://stackoverflow.com/a/148473/9302545
  sed -i "0,/	     \"solid-fenics/s//	plot \"solid-fenics/" plot-all-displacements.sh
  cat plot-all-displacements.sh
  ./plot-all-displacements.sh solid-fenics >${CI_PROJECT_DIR}/log/plot_displacement_tutorial_pf_openfoam-com_fenicsprecice_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').ps
  ls -l
  epspdf ${CI_PROJECT_DIR}/log/*.ps
  rm ${CI_PROJECT_DIR}/log/*.ps
  popd
  sudo pacman -Rscn --noconfirm gnuplot texlive-pictures >/dev/null 2>&1
}

function log() {
  mkdir -p ${CI_PROJECT_DIR}/log
  run_elastic_tube_3d 2>&1 | tee -a ${CI_PROJECT_DIR}/log/log_tutorial_et3_openfoam-com_fenicsprecice_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null
  tar -c -I 'zstd -19 -T0' -f ${CI_PROJECT_DIR}/log/output_tutorial_et3_openfoam-com_fenicsprecice_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').tar.zst ${CI_PROJECT_DIR}/tutorials/elastic-tube-3d/{solid-fenics,fluid-openfoam}
  # ls -l ${CI_PROJECT_DIR}/log/*.log
}

function visualize() {
  # https://raw.githubusercontent.com/precice/vm/main/provisioning/.alias
  # TODO: Check SC2002 (style): Useless cat. Consider 'cmd < file | ..' or 'cmd file | ..' instead.
  local CONFIG_PATH="${CI_PROJECT_DIR}"/tutorials/elastic-tube-3d/precice-config.xml
  precice-tools check ${CONFIG_PATH}
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpdf >${CI_PROJECT_DIR}/log/precice-config.pdf
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tsvg >${CI_PROJECT_DIR}/log/precice-config.svg
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpng >${CI_PROJECT_DIR}/log/precice-config.png
}

curl -s https://gitlab.com/dune-archiso/testing/precice-arch-scripts/-/raw/main/install_openfoam-com.sh | bash
install
clone_tutorials
run_elastic_tube_3d
log
plot_displacement
visualize
ls "${CI_PROJECT_DIR}"/log/*.log "${CI_PROJECT_DIR}"/log/*.pdf "${CI_PROJECT_DIR}"/log/*.png "${CI_PROJECT_DIR}"/log/*.svg
