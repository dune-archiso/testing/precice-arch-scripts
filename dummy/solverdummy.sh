#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2002

function install() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  sudo pacman --needed --noconfirm -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst >/dev/null 2>&1
}

function run_solverdummies() {
  sudo pacman --needed --noconfirm -S cmake gcc-fortran >/dev/null 2>&1
  cp -R /usr/share/precice/examples/solverdummies "${CI_PROJECT_DIR}"

  cmake -S "${CI_PROJECT_DIR}"/solverdummies/c -B "${CI_PROJECT_DIR}"/build-c
  cmake --build "${CI_PROJECT_DIR}"/build-c

  cmake -S "${CI_PROJECT_DIR}"/solverdummies/cpp -B "${CI_PROJECT_DIR}"/build-cpp
  cmake --build "${CI_PROJECT_DIR}"/build-cpp

  cmake -S "${CI_PROJECT_DIR}"/solverdummies/fortran -B "${CI_PROJECT_DIR}"/build-fortran
  cmake --build "${CI_PROJECT_DIR}"/build-fortran

  curl -O https://raw.githubusercontent.com/precice/precice/develop/cmake/runsolverdummies.sh >/dev/null 2>&1
  chmod u+x runsolverdummies.sh
  sudo mv runsolverdummies.sh /usr/bin

  runsolverdummies.sh "${CI_PROJECT_DIR}"/build-c/solverdummy "${CI_PROJECT_DIR}"/build-c/solverdummy "${CI_PROJECT_DIR}"/solverdummies/precice-config.xml

  runsolverdummies.sh "${CI_PROJECT_DIR}"/build-cpp/solverdummy "${CI_PROJECT_DIR}"/build-cpp/solverdummy "${CI_PROJECT_DIR}"/solverdummies/precice-config.xml

  runsolverdummies.sh "${CI_PROJECT_DIR}"/build-fortran/solverdummy "${CI_PROJECT_DIR}"/build-fortran/solverdummy "${CI_PROJECT_DIR}"/solverdummies/precice-config.xml

  sudo pacman -Rscn --noconfirm cmake gcc-fortran >/dev/null 2>&1
}

function log() {
  mkdir -p "${CI_PROJECT_DIR}"/log
  run_solverdummies 2>&1 | tee -a "${CI_PROJECT_DIR}"/log/log_solverdummy_"$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago')".log >/dev/null
  # ls -l ${CI_PROJECT_DIR}/log/*.log
}

function visualize() {
  # https://raw.githubusercontent.com/precice/vm/main/provisioning/.alias
  local CONFIG_PATH="${CI_PROJECT_DIR}"/solverdummies/precice-config.xml
  # ldd /usr/bin/precice-tools
  # precice-tools --help
  precice-tools check "${CONFIG_PATH}"
  cat "${CONFIG_PATH}" | precice-config-visualizer | dot -Tpdf >"${CI_PROJECT_DIR}"/log/precice-config.pdf
  cat "${CONFIG_PATH}" | precice-config-visualizer | dot -Tsvg >"${CI_PROJECT_DIR}"/log/precice-config.svg
  cat "${CONFIG_PATH}" | precice-config-visualizer | dot -Tpng >"${CI_PROJECT_DIR}"/log/precice-config.png
}

install
log
visualize
ls "${CI_PROJECT_DIR}"/log/*.log "${CI_PROJECT_DIR}"/log/*.pdf "${CI_PROJECT_DIR}"/log/*.png "${CI_PROJECT_DIR}"/log/*.svg
