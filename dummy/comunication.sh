#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

function comunication_python() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  sudo pacman --needed --noconfirm -S python >/dev/null 2>&1

  local BASE_URL=https://gitlab.com/dune-archiso/testing/precice-arch/-/raw/main/tools
  curl -s ${BASE_URL}/comunication_A.py | python &
  local PID_A=$!
  curl -s ${BASE_URL}/comunication_B.py | python &
  local PID_B=$!

  wait "${PID_A}" "${PID_B}"
  sudo pacman -Rscn --noconfirm python >/dev/null 2>&1
}

comunication_python
