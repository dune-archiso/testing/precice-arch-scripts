#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2086,SC2125

function download_assets() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  sudo pacman --needed --noconfirm -S github-cli >/dev/null 2>&1
  cp "${GH_TOKEN}" mytoken.txt
  export GH_TOKEN=
  gh auth login --with-token <mytoken.txt
  # gh auth status
  local _openfoam_version=v2312
  local PACKAGE=openfoam-com-"${_openfoam_version}"-*-x86_64.pkg.tar.zst
  # https://man.archlinux.org/man/gh.1
  # https://cli.github.com/manual/gh_release_download
  gh release download --repo github.com/carlosal1015/aur --pattern "${PACKAGE}"
  rm ~/.config/gh/hosts.yml
  file ${PACKAGE}
  du -sh ${PACKAGE}
  mkdir -p "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  mv ${PACKAGE} "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  sudo pacman -Rscn --noconfirm github-cli >/dev/null 2>&1
}

function install_openfoam-com() {
  local _openfoam_version=v2312
  local PACKAGE=openfoam-com-"${_openfoam_version}"-*-x86_64.pkg.tar.zst
  sudo pacman --needed --noconfirm -S hdf5-openmpi netcdf-openmpi >/dev/null 2>&1
  sudo pacman --needed --noconfirm -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/${PACKAGE} >/dev/null 2>&1
}

curl -s https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/add_arch4edu.sh | bash
download_assets
install_openfoam-com
