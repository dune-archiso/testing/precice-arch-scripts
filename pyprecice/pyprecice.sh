#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2086

function install() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  sudo pacman -U --needed "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/*.pkg.tar.zst --noconfirm >/dev/null 2>&1
}

function run_generator_propagator() {
  sudo pacman --needed --noconfirm -S python-matplotlib git >/dev/null 2>&1 # openssh

  local BASE_URL=https://github.com/MakisH
  local PYPRECICE="${BASE_URL}"/ofw15-slides

  git clone \
    -q \
    --filter=blob:none \
    --no-checkout \
    --depth=1 \
    --sparse \
    --branch master \
    "${PYPRECICE}".git

  pushd ofw15-slides || exit

  git sparse-checkout set generator-propagator/solution
  git checkout

  pushd generator-propagator/solution || exit
  python generator.py &
  local PID_A=$!
  python propagator.py &
  local PID_B=$!

  wait "${PID_A}" "${PID_B}"
  popd || exit

  sudo pacman -Rscn --noconfirm python-matplotlib git >/dev/null 2>&1 # openssh
}

function log() {
  mkdir -p "${CI_PROJECT_DIR}"/log
  run_generator_propagator 2>&1 | tee -a "${CI_PROJECT_DIR}"/log/log_generator_propagator_"$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago')".log >/dev/null
  # ls -l ${CI_PROJECT_DIR}/log/*.log
}

function visualize() {
  # https://raw.githubusercontent.com/precice/vm/main/provisioning/.alias
  local CONFIG_PATH="${CI_PROJECT_DIR}"/ofw15-slides/generator-propagator/solution/precice-config.xml
  precice-tools check ${CONFIG_PATH}
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpdf >${CI_PROJECT_DIR}/log/precice-config.pdf
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tsvg >${CI_PROJECT_DIR}/log/precice-config.svg
  cat ${CONFIG_PATH} | precice-config-visualizer | dot -Tpng >${CI_PROJECT_DIR}/log/precice-config.png
}

install
log
visualize
ls "${CI_PROJECT_DIR}"/log/*.log "${CI_PROJECT_DIR}"/log/*.pdf "${CI_PROJECT_DIR}"/log/*.png "${CI_PROJECT_DIR}"/log/*.svg
