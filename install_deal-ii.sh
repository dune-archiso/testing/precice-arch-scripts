#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2086,SC2125

function download_assets() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  sudo pacman --needed --noconfirm -S github-cli >/dev/null 2>&1
  cp "${GH_TOKEN}" mytoken.txt
  export GH_TOKEN=
  gh auth login --with-token <mytoken.txt
  # gh auth status
  # local _trilinos_version=15.0.0
  local _dealii_version=9.5.1
  local TRILINOS_PACKAGE=trilinos-*-x86_64.pkg.tar.zst
  local DEAL_II_PACKAGE=deal-ii-"${_dealii_version}"-*-x86_64.pkg.tar.zst
  # https://man.archlinux.org/man/gh.1
  # https://cli.github.com/manual/gh_release_download
  gh release download --repo github.com/carlosal1015/aur --pattern "${TRILINOS_PACKAGE}"
  gh release download --repo github.com/carlosal1015/aur --pattern "${DEAL_II_PACKAGE}"
  ls
  rm ~/.config/gh/hosts.yml mytoken.txt
  file ${TRILINOS_PACKAGE} ${DEAL_II_PACKAGE}
  du -sh ${TRILINOS_PACKAGE} ${DEAL_II_PACKAGE}
  mkdir -p "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  mv ${TRILINOS_PACKAGE} "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  mv ${DEAL_II_PACKAGE} "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  sudo pacman -Rscn --noconfirm github-cli >/dev/null 2>&1
}

function install_deal-ii() {
  # local _trilinos_version=15.0.0
  local _dealii_version=9.5.1
  local TRILINOS_PACKAGE=trilinos-*-x86_64.pkg.tar.zst
  local DEAL_II_PACKAGE=deal-ii-"${_dealii_version}"-*-x86_64.pkg.tar.zst
  sudo pacman --needed --noconfirm -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/${TRILINOS_PACKAGE} >/dev/null 2>&1
  sudo pacman --needed --noconfirm -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/${DEAL_II_PACKAGE} >/dev/null 2>&1
}

download_assets
install_deal-ii
