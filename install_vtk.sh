#!/usr/bin/env bash

# Copyright (C) 2023 Carlos Aznarán <caznaranl@uni.pe>

# This file is part of https://gitlab.com/dune-archiso/testing/precice-arch .
# https://gitlab.com/dune-archiso/testing/precice-arch is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# https://gitlab.com/dune-archiso/testing/precice-arch is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with https://gitlab.com/dune-archiso/testing/precice-arch .  If not, see <http://www.gnu.org/licenses/>.

# shellcheck disable=SC2086,SC2125

function download_assets() {
  sudo pacman --needed --noconfirm -Syuq >/dev/null 2>&1
  sudo pacman --needed --noconfirm -S github-cli >/dev/null 2>&1
  cp "${GH_TOKEN}" mytoken.txt
  export GH_TOKEN=
  gh auth login --with-token <mytoken.txt
  local BOOST_PACKAGE=boost-*-x86_64.pkg.tar.zst
  local LIBLAS_PACKAGE=liblas-*-x86_64.pkg.tar.zst
  local VTK_PACKAGE=vtk-*-x86_64.pkg.tar.zst
  gh release download --repo github.com/carlosal1015/aur --pattern "${BOOST_PACKAGE}"
  gh release download --repo github.com/carlosal1015/aur --pattern "${LIBLAS_PACKAGE}"
  gh release download --repo github.com/carlosal1015/aur --pattern "${VTK_PACKAGE}"
  ls
  rm ~/.config/gh/hosts.yml mytoken.txt
  file ${BOOST_PACKAGE} ${LIBLAS_PACKAGE} ${VTK_PACKAGE}
  du -sh ${BOOST_PACKAGE} ${LIBLAS_PACKAGE} ${VTK_PACKAGE}
  mkdir -p "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  mv ${BOOST_PACKAGE} "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  mv ${LIBLAS_PACKAGE} "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  mv ${VTK_PACKAGE} "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"
  sudo pacman -Rscn --noconfirm github-cli >/dev/null 2>&1
}

function install_vtk() {
  local BOOST_PACKAGE=boost-*-x86_64.pkg.tar.zst
  local LIBLAS_PACKAGE=liblas-*-x86_64.pkg.tar.zst
  local VTK_PACKAGE=vtk-*-x86_64.pkg.tar.zst
  sudo pacman --needed --noconfirm -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/${BOOST_PACKAGE} >/dev/null 2>&1
  sudo pacman --needed --noconfirm -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/${LIBLAS_PACKAGE} >/dev/null 2>&1
  sudo pacman --needed --noconfirm -U "${CI_PROJECT_DIR}"/"${ARCHITECTURE}"/${VTK_PACKAGE} >/dev/null 2>&1
}

download_assets
install_vtk
